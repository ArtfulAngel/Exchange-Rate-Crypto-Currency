import React, { Component } from 'react';
import './Ticker.css';

export default class Ticker extends Component {
    state = {
        value: 0,
    };

    componentDidMount() {
        this.updateCurrencyPrice();
        this.interval = setInterval(this.updateCurrencyPrice, 10000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    updateCurrencyPrice = () => {
        return fetch(`https://api.coinmarketcap.com/v1/ticker/${this.props.currency.id}/`)
            .then(r => r.json())
            .then(res => {
                this.setState({
                    value: parseFloat(res.map(curr => curr.price_usd)),
                });
            });
    };

    render() {
        const { currency } = this.props;
        return (
            <div className="ticker">
                <p className="ticker__name">{`${currency.name}`}</p>
                <p>{`${currency.symbol} to USD`}</p>
                <p className="ticker__price">{this.state.value.toFixed(2)}</p>
            </div>
        );
    }
}
