import React, { Component } from 'react';
import './App.css';
import Ticker from './Ticker/Ticker';

class App extends Component {
    state = {
        activeCurrency: ['bitcoin', 'litecoin'],
        currency: [],
    };

    componentDidMount() {
        this.requestListNameCurrency();
    }

    requestListNameCurrency = () => {
        return fetch(`https://api.coinmarketcap.com/v1/ticker/`)
            .then(r => r.json())
            .then(res => {
                this.setState({
                    currency: res.map(curr => curr),
                });
            });
    };

    handleCheckCurrency = currency => event => {
        const { checked } = event.target;
        this.setState(({ activeCurrency }) => {
            let newActivCurr = [...activeCurrency];

            if (checked) {
                newActivCurr.push(currency);
            } else {
                newActivCurr = newActivCurr.filter(curr => curr !== currency);
            }

            return {
                activeCurrency: newActivCurr,
            };
        });
    };

    render() {
        return (
            <div className="app">
                <aside>
                    <ul>
                        {this.state.currency.map(curr => (
                            <li key={curr.id} className="currItem">
                                <input
                                    type="checkbox"
                                    id={curr.id}
                                    onChange={this.handleCheckCurrency(curr.id)}
                                    checked={this.state.activeCurrency.includes(curr.id)}
                                />
                                <label htmlFor={curr.id}>{`${curr.name} (${curr.symbol})`}</label>
                            </li>
                        ))}
                    </ul>
                </aside>

                <main>
                    {this.state.currency.map(
                        curr =>
                            this.state.activeCurrency.includes(curr.id) ? (
                                <Ticker
                                    key={curr.id}
                                    currency={curr}
                                    isActive={this.state.activeCurrency.includes(curr.id)}
                                />
                            ) : null,
                    )}
                </main>
            </div>
        );
    }
}

export default App;
